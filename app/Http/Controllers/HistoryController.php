<?php

namespace App\Http\Controllers;

use App\Models\History;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()) {
            $user = Auth::user()->is_admin;
            if($user == 1) {
                return view ('histories.index', ['histories' => History::paginate(5)]);
            } else {
                $user = Auth::user();
                $histories = $user->history->paginate(5);
                return view('histories.index', compact('histories'));
            }
        } else {
            $histories = History::paginate(5);

            return view('histories.index', compact('histories'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('histories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'title'          => 'required',
            'description'    => 'required',
            'user_id'      => 'required'
        ]);


        History::create($request->all());

        return redirect()->route('histories.index')
            ->with('success', 'History created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\History  $history
     * @return \Illuminate\Http\Response
     */
    public function show(History $history)
    {
        return view('histories.show', compact('history'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\History  $history
     * @return \Illuminate\Http\Response
     */
    public function edit(History $history)
    {
        return view('histories.edit', compact('history'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\History  $history
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, History $history)
    {
        $request->validate([
            'title'          => 'required',
            'description'    => 'required',
        ]);

        $history->update($request->all());

        return redirect()->route('histories.index')
            ->with('success', 'History updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\History  $history
     * @return \Illuminate\Http\Response
     */
    public function destroy(History $history)
    {
        $history->delete();

        return redirect()->route('histories.index')
            ->with('success', 'History deleted successfully');
    }
}
