<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class HistoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $histories = [];

        for ($i = 1; $i <= 100; $i++) {
            $histories[] = [
                'title'           => Str::random(10),
                'description'     => Str::random(10),
                'user_name'       => Str::random(10),
                'user_id'         => rand(1, 100),
            ];
        }

        DB::table('histories')->insert($histories);
    }
}
