@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>My Histories</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('histories.create') }}" title="Create a product"> <i class="fas fa-plus-circle"></i>
                </a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered table-responsive-lg">
        <tr>
            <th>Title</th>
            <th>Description</th>
            <th>User Name</th>
            <th>Date Created</th>
            <th>Actions</th>
        </tr>
        @foreach ($histories as $history)
            <tr>
                <td>{{ $history->title }}</td>
                <td>{{ $history->description }}</td>
                <td>{{ $history->user->name }}</td>
                <td>{{ $history->created_at }}</td>
                <td>
                    <form action="{{ route('histories.destroy', $history) }}" method="POST">

                        <a href="{{ route('histories.show', $history) }}" title="show">
                            <i class="fas fa-eye text-success  fa-lg"></i>
                        </a>

                        <a href="{{ route('histories.edit', $history) }}">
                            <i class="fas fa-edit  fa-lg"></i>
                        </a>

                        @csrf
                        @method('DELETE')

                        <button type="submit" title="delete" style="border: none; background-color:transparent;">
                            <i class="fas fa-trash fa-lg text-danger"></i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

    {!! $histories->links() !!}

@endsection
